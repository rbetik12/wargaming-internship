#pragma once
#include <thread>
#include <mutex>

template<typename T>
class SafeBuffer {
public:
    SafeBuffer<T>(std::size_t size): size(size), headIndex(0), tailIndex(0) {
        elements = new T[size];
    }

    void Push(T& e) {
        std::lock_guard<std::mutex> lock(mutex);
        if (headIndex >= size) {
            tailIndex = (tailIndex + 1) % size;
        }
        headIndex = headIndex % size;
        elements[headIndex] = e;
        headIndex += 1;
    }

    T* Pop() {
        std::lock_guard<std::mutex> lock(mutex);
        T* data = &elements[tailIndex];
        tailIndex = (tailIndex + 1) % size;
        return data;
    }

    std::size_t Size() {
        std::lock_guard<std::mutex> lock(mutex);
        return size - headIndex;
    }

    std::size_t Capacity() {
        return size;
    }

    void Clear() {
        std::lock_guard<std::mutex> lock(mutex);
        headIndex = 0;
        tailIndex = 0;
    }
private:
    T* elements;
    std::size_t headIndex;
    std::size_t tailIndex;
    std::size_t size;
    std::mutex mutex;
};
