#include "Buffer.h"
#include <iostream>

Buffer::Buffer(std::size_t size) : size(size), headIndex(0), tailIndex(0) {
    elements = new BufferElement[size];
}

Buffer::~Buffer() {
    delete[] elements;
}

void Buffer::Push(void* data) {
    if (headIndex >= size) {
        tailIndex = (tailIndex + 1) % size;
    }
    headIndex = headIndex % size;
    elements[headIndex].data = data;
    headIndex += 1;
}

void* Buffer::Pop() {
    void* data = elements[tailIndex].data;
    tailIndex = (tailIndex + 1) % size;
    return data;
}

std::size_t Buffer::Size() {
    return size - headIndex;
}

std::size_t Buffer::Capacity() {
    return size;
}

void Buffer::Clear() {
    headIndex = 0;
    tailIndex = 0;
}
