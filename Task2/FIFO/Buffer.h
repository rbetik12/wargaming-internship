#pragma once
#include <cstddef>

struct BufferElement {
    void* data;
};

class Buffer {
public:
    Buffer(std::size_t size);
    ~Buffer();
    void Push(void * data);
    void* Pop();
    std::size_t Size();
    std::size_t Capacity();
    void Clear();
private:
    BufferElement* elements;
    std::size_t headIndex;
    std::size_t tailIndex;
    std::size_t size;
};
