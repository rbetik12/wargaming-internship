#include <iostream>
#include "FIFO/Buffer.h"
#include "FIFO/SafeBuffer.h"
#include <string>

struct Test {
    int kek = 4;
    long long lol = 23;
    std::string string = "Hello!";
};

void writeToBuffer(SafeBuffer<std::string>& buf, std::size_t amount) {
    for (std::size_t i = 0; i < amount; i++) {
        buf.Push(std::to_string(i));
    }
}

int main() {
    std::cout << "================Testing buffer================" << std::endl;
    Buffer buffer(5);
    Test test[5];

    std::cout << "Input:" << std::endl;
    for (int i = 0; i < 5; i++) {
        test[i].string += std::to_string(i);
        buffer.Push(&test[i]);
        std::cout << &test[i] << ": ";
        std::cout << test[i].string << std::endl;
    }

    std::cout << "Output:" << std::endl;

    Test* testFromBuffer;
    for (int i = 0; i < 4; i++) {
        testFromBuffer = (Test*)buffer.Pop();
        std::cout << testFromBuffer->string << std::endl;
    }

    std::cout << "================Buffer overflow================" << std::endl;
    buffer.Clear();
    buffer.Push(&test[0]);
    buffer.Push(&test[1]);
    buffer.Push(&test[2]);
    buffer.Push(&test[3]);
    buffer.Push(&test[4]);
    buffer.Push(&test[3]);

    for (int i = 0; i < 5; i++) {
        testFromBuffer = (Test*)buffer.Pop();
        std::cout << testFromBuffer->string << std::endl;
    }

    std::cout << "================Testing thread safe buffer================" << std::endl;

    SafeBuffer<std::string> safeBuffer(5);
    std::string strings[5] = {
        "lol",
        "kek",
        "chebyrek",
        "kek",
        "lol"
    };

    std::cout << "Input:" << std::endl;
    for (int i = 0; i < 5; i++) {
        std::cout << strings[i] << std::endl;
        safeBuffer.Push(strings[i]);
    }

    std::cout << std::endl;

    std::cout << "Output:" << std::endl;
    for (int i = 0; i < 5; i++) {
        std::cout << *safeBuffer.Pop() << std::endl;
    }

    std::cout << "================Buffer overflow================" << std::endl;
    safeBuffer.Clear();

    std::thread thread1(writeToBuffer, std::ref(safeBuffer), 3);
    std::thread thread2(writeToBuffer, std::ref(safeBuffer), 5);

    thread1.join();
    thread2.join();

    for (int i = 0; i < 5; i++) {
        std::cout << *safeBuffer.Pop() << std::endl;
    }

    return 0;
}