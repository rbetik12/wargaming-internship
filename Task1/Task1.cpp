﻿#include <iostream>
#include <cstdlib>
#include <chrono>

using namespace std::chrono;

bool IsEvenFast(long long value) {
    return !(value & 1);
}

bool IsEven(long long value) {
    return value % 2 == 0;
}

int main() {
    while (true) {
        int number;
        std::cin >> number;

        //Execution time comparing
        /*auto start = high_resolution_clock::now();
        IsEvenFast(number);
        auto finish = high_resolution_clock::now();
        auto time = duration_cast<nanoseconds>(finish - start);
        std::cout << "Bit mask: " << time.count() << " ns" << std::endl;

        auto start1 = high_resolution_clock::now();
        IsEven(number);
        auto finish1 = high_resolution_clock::now();
        auto time1 = duration_cast<nanoseconds>(finish1 - start1);
        std::cout << "Naive: " << time1.count() << " ns" << std::endl;*/

        if (IsEvenFast(number)) {
            std::cout << "Even!" << std::endl;
        }
        else {
            std::cout << "Odd!" << std::endl;
        }
    }
    return 0;
}
