#include <iostream>
#include <random>
#include <chrono>

#define ARRAY_SIZE 10

void FillArray(int* array, size_t size) {
    for (size_t i = 0; i < size; i++) {
        array[i] = rand() - RAND_MAX / 2;
    }
}

void PrintArray(int* array, size_t size) {
    for (size_t i = 0; i < size; i++) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

std::pair<int, int> Divide(int* arr, int left, int right) {
    int mid = left;
    int pivot = arr[right];

    while (mid <= right) {
        if (arr[mid] < pivot) {
            std::swap(arr[left], arr[mid]);
            ++left, ++mid;
        }
        else if (arr[mid] > pivot) {
            std::swap(arr[mid], arr[right]);
            --right;
        }
        else {
            ++mid;
        }
    }

    return std::make_pair(left - 1, mid);
}

void QuickSort(int* array, int left, int right) {
    if (left - right == 1) {
        if (array[left] < array[right])
            std::swap(array[left], array[right]);
        return;
    }

    if (left < right) {
        std::pair<int, int> pair = Divide(array, left, right);

        QuickSort(array, left, pair.first);
        QuickSort(array, pair.second, right);
    }
}

void Sort(int* array, size_t size) {
    QuickSort(array, 0, size - 1);
}

int main() {
    int* array = new int[ARRAY_SIZE];
    FillArray(array, ARRAY_SIZE);
    PrintArray(array, ARRAY_SIZE);
    auto start1 = std::chrono::high_resolution_clock::now();
    Sort(array, ARRAY_SIZE);
    auto finish1 = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(finish1 - start1);
    std::cout << "Quick sort: " << time.count() << " ns" << std::endl;

    PrintArray(array, ARRAY_SIZE);

    return 0;
}